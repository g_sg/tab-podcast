const { getLinks } = require('./src/scraper');
const { makeFeed } = require('./src/feed');

const { upload } = require('./src/s3');
const fs = require('fs');

function getKeyName(link) {
  const parts = link.split('/');
  return parts[parts.length - 1];
}

function fixUrlProto(url) {
  return url.replace(/^.*\/\//, 'https://');
}

(async () => {
  const links = await getLinks();

  const items = links.map(async link => ({
    ...link, url: await upload(fixUrlProto(link.url), getKeyName(link.url))
  }));
  const promised = await Promise.all(items);
  const xml = makeFeed(promised).xml();

  fs.writeFile('./dist/feed.xml', xml, err => {
    if (err) {
      throw err;
    }
    console.log('wrote dist/feed.xml');
  });
})();
