const axios = require('axios');
const cheerio = require('cheerio');
const moment = require('moment');

const ENTRY_PAGE_URL = 'https://metropolitantabernacle.org';
const SERMON_DATE_SEPARATOR = '.';
const SERMON_DATE_FORMAT = 'D MMM YYYY';

const SELECTORS = {
  url: '.latest-sermon-audio > a',
  date: '.latest-item-date',
  title: 'h2',
  description: '.latest-sermon-desc',
  author: '.latest-sermon-text-preacher > a'
};


function getEntryPage() {
  return axios.get(ENTRY_PAGE_URL);
}

async function getCheerioObject() {
  const { data } = await getEntryPage();
  return cheerio.load(data);
}

function parseSermonDate(text) {
  const dateStr = text.split(SERMON_DATE_SEPARATOR)[1]
        .trim();
  return moment(dateStr, SERMON_DATE_FORMAT).format();
}

async function getLinks() {
  const $ = await getCheerioObject();
  const latest = $('.latest-sermon');
  const links = latest.map((_, el) => {
    const $el = $(el);

    const dateStr = $el.find(SELECTORS.date).text();
    const date = parseSermonDate(dateStr);

    const title = $el.find(SELECTORS.title).text();
    const description = $el.find(SELECTORS.description)
          .text().trim();
    const url = $el.find(SELECTORS.url).attr('href');
    const author = $el.find(SELECTORS.author).text();

    return {
      title, date, description, url, author
    };
  }).toArray();

  return links;
}

module.exports = { getLinks };
