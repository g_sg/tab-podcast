const RSS = require('rss');

const coverUrl = 'https://tab.ggoncalves.me/cover.jpg';
const feed = new RSS({
  title: 'Metropolitan Tabernacle sermons',
  description: 'Sermons from the Metropolitan Tabernacle in London',
  feed_url: 'https://tab.ggoncalves.me/feed.xml',
  site_url: 'https://metropolitantabernacle.org',
  image_url: coverUrl,
  docs: 'https://tab.ggoncalves.me/docs',
  managingEditor: 'Guilherme Goncalves <tab@gclv.es>',
  webMaster: 'Guilherme Goncalves <tab@gclv.es>',
  copyright: '2019 Dr. Peter Masters',
  language: 'en',
  pubDate: new Date(),
  ttl: 3600,                  // XXX is this minutes?
  custom_namespaces: {
    'itunes': 'http://www.itunes.com/dtds/podcast-1.0.dtd'
  },
  custom_elements: [
    {'itunes:author': 'Metropolitan Tabernacle'},
    {'itunes:summary': 'Sermons from the Metropolitan Tabernacle in London'},
    {'itunes:explicit': 'no'},
    {'itunes:owner': [
      {'itunes:name': 'Guilherme Goncalves'},
      {'itunes:email': 'tab@gclv.es'}
    ]},
    {'itunes:image': {
      _attr: {
        href: coverUrl
      }
    }},
    {'itunes:category': [
      {_attr: {
        text: 'Religion & Spirituality'
      }},
      {'itunes:category': {
        _attr: {
          text: 'Christianity'
        }
      }}
    ]}
  ]
});

function makeFeed(items) {
  items.forEach(({ title, date, description, url, author }) => {
    feed.item({
      title, description, url, author,
      pubDate: date,
      enclosure: { url },
      custom_elements: [
        {'itunes:author': author },
        {'itunes:subtitle': title},
        {'itunes:summary': description},
        {'itunes:image': {
          _attr: {
            href: coverUrl
          }
        }},
      ]
    });
  });

  return feed;
}

module.exports = { feed, makeFeed };
