const S3 = require('aws-sdk/clients/s3');
const axios = require('axios');

const s3 = new S3({
  region: 'eu-west-2', apiVersion: '2006-03-01',
  params: { Bucket: 'metropolitan-tabernacle-podcast' }
});

const MAX_RETRIES = 3;
const attempts = { };

async function upload(url, key) {
  console.log(`uploading contents from ${url} to item with key ${key}`);
  const response = await axios({
    url,
    method: 'get',
    responseType: 'stream'
  });

  const attemptNumber = incrementRetriesForUrl(url);
  attempts[url] = attemptNumber;

  try {
    const { Location } = await s3.upload({
      Body: response.data,
      Key: key,
    }, console.log).promise();

    return Location;
  } catch (e) {
    if (attemptNumber <= MAX_RETRIES) {
      console.warn(`Caught error ${e}. Retrying... [${attemptNumber}/${MAX_RETRIES}]`);
      return upload(url, key);
    }
    throw e;
  }
}

module.exports = { upload };

function incrementRetriesForUrl(url) {
  return 1 + (attempts[url] || 0);
}
